"""Yaml wrapper interaction tests."""
import io
import unittest

from cki_lib import yaml_wrap
from cki_lib.misc import tempfile_from_string


class TestYamlWrap(unittest.TestCase):
    """Test yaml_wrap class."""

    def test_cannot_inst_serializer(self):
        # pylint: disable=W0223,E0110
        """Ensure Serializer is an interface."""
        class BadSerializer(yaml_wrap.Serializer):
            """Fake serializer without proper methods implemented."""

        with self.assertRaises(TypeError):
            BadSerializer()

    def test_ruamel(self):
        """Ensure RuamelSerializer's serialize/deserialize works."""
        yaml_example = b'no: go'
        serializer = yaml_wrap.RuamelSerializer()

        with tempfile_from_string(yaml_example) as fpath:
            test_object = serializer.deserialize_file(fpath)

        iobj = io.BytesIO()
        serializer.serialize(test_object, iobj)
        iobj.seek(0)

        self.assertEqual(yaml_example + b'\n', iobj.read())
