"""Yaml util tests."""
import unittest

import yaml

from cki_lib.yaml import BlockDumper


class TestYaml(unittest.TestCase):
    """Test yaml utils."""

    @staticmethod
    def _dump(what):
        return yaml.dump(what, Dumper=BlockDumper)

    def test_normal_string(self):
        """Check block string dumping without line feeds."""
        self.assertEqual(self._dump({"key": "value"}), "key: value\n")

    def test_line_feed(self):
        """Check block string dumping with line feeds."""
        self.assertEqual(self._dump({"key": "value\nline 2"}),
                         "key: |-\n  value\n  line 2\n")

    def test_valid_data(self):
        """Check roundtripping some data."""
        data = {"a": "b",
                "c": ["d", {"e": "f\ng", "h": "i", "k\nl": ["m"]}]}
        self.assertEqual(data, yaml.safe_load(self._dump(data)))
