"""Test MessageQueue."""
import itertools
import json
import os
import signal
import ssl
import threading
import time
import unittest
from unittest import mock

import pika

from cki_lib import messagequeue


class TestMessageQueue(unittest.TestCase):
    # pylint: disable=too-many-public-methods
    """Test cki_lib/messagequeue.py."""

    def test_init(self):
        """Test MessageQueue init."""
        queue = messagequeue.MessageQueue('host/', 123, 'user', 'password')
        credentials = pika.PlainCredentials('user', 'password')
        connection_params = [pika.ConnectionParameters(
            host='host', port=123, credentials=credentials
        )]

        self.assertEqual(queue.credentials, credentials)
        self.assertEqual(queue.connection_params, connection_params)

    @mock.patch.dict(os.environ, {
        'RABBITMQ_HOST': 'host',
        'RABBITMQ_PORT': '123',
        'RABBITMQ_USER': 'user',
        'RABBITMQ_PASSWORD': 'password',
    })
    def test_init_env_default(self):
        """Test MessageQueue default values from env variables."""
        queue = messagequeue.MessageQueue()
        credentials = pika.PlainCredentials('user', 'password')
        connection_params = [pika.ConnectionParameters(
            host='host', port=123, credentials=credentials
        )]

        self.assertEqual(queue.credentials, credentials)
        self.assertEqual(queue.connection_params, connection_params)

    @mock.patch.dict(os.environ, {
        'RABBITMQ_HOST': 'host-ignored',
        'RABBITMQ_PORT': '1234',
        'RABBITMQ_USER': 'user-ignored',
        'RABBITMQ_PASSWORD': 'password-ignored',
    })
    def test_init_env_default_ignored(self):
        """Test MessageQueue ignoring default values from env variables."""
        queue = messagequeue.MessageQueue('host', 123, 'user', 'password')
        credentials = pika.PlainCredentials('user', 'password')
        connection_params = [pika.ConnectionParameters(
            host='host', port=123, credentials=credentials
        )]

        self.assertEqual(queue.credentials, credentials)
        self.assertEqual(queue.connection_params, connection_params)

    def test_init_default(self):
        """Test MessageQueue init default values."""
        queue = messagequeue.MessageQueue('host')
        credentials = pika.PlainCredentials('guest', 'guest')
        connection_params = [pika.ConnectionParameters(
            host='host', port=5672, credentials=credentials
        )]

        self.assertEqual(queue.credentials, credentials)
        self.assertEqual(queue.connection_params, connection_params)

    def test_init_ssl_443(self):
        """Test MessageQueue init default values with ssl."""
        queue = messagequeue.MessageQueue('host', port=443)
        credentials = pika.PlainCredentials('guest', 'guest')
        ssl_options = pika.SSLOptions(ssl.create_default_context())
        connection_params = [pika.ConnectionParameters(
            host='host', port=443, credentials=credentials,
            ssl_options=ssl_options
        )]

        self.assertEqual(queue.credentials, credentials)
        self.assertEqual(queue.connection_params, connection_params)

    def test_init_ssl_5671(self):
        """Test MessageQueue init default values with ssl."""
        queue = messagequeue.MessageQueue('host', port=5671)
        credentials = pika.PlainCredentials('guest', 'guest')
        ssl_options = pika.SSLOptions(ssl.create_default_context())
        connection_params = [pika.ConnectionParameters(
            host='host', port=5671, credentials=credentials,
            ssl_options=ssl_options
        )]

        self.assertEqual(queue.credentials, credentials)
        self.assertEqual(queue.connection_params, connection_params)

    @mock.patch('ssl.SSLContext.load_cert_chain')
    def test_init_ssl_cert(self, load_cert_chain):
        """Test MessageQueue init with ssl cert."""
        certfile = 'certfile'
        queue = messagequeue.MessageQueue(
            'host', port=443, certfile=certfile)
        credentials = pika.credentials.ExternalCredentials()
        ssl_options = pika.SSLOptions(ssl.create_default_context())
        connection_params = [pika.ConnectionParameters(
            host='host', port=443, credentials=credentials,
            ssl_options=ssl_options
        )]

        self.assertEqual(queue.credentials, credentials)
        self.assertEqual(queue.connection_params, connection_params)
        load_cert_chain.assert_called_with(certfile)

    def test_init_ssl_custom(self):
        """Test MessageQueue init default values with custom ssl."""
        ssl_options = pika.SSLOptions(ssl.create_default_context(), 'host')
        queue = messagequeue.MessageQueue(
            'host', port=443, connection_params={'ssl_options': ssl_options})
        credentials = pika.PlainCredentials('guest', 'guest')
        connection_params = [pika.ConnectionParameters(
            host='host', port=443, credentials=credentials,
            ssl_options=ssl_options
        )]

        self.assertEqual(queue.credentials, credentials)
        self.assertEqual(queue.connection_params, connection_params)

    @staticmethod
    def _sorted_connections(connections):
        return sorted(connections, key=lambda c: c.host)

    def _test_init_multiple(self, param, hosts):
        queue = messagequeue.MessageQueue(param)
        credentials = pika.PlainCredentials('guest', 'guest')
        connection_params = [pika.ConnectionParameters(
            host=h, port=5672, credentials=credentials
        ) for h in hosts]

        self.assertEqual(queue.credentials, credentials)
        self.assertEqual(queue.connection_params, connection_params)

        with mock.patch('pika.BlockingConnection') as connection:
            with queue.connect():
                pass
        seen = self._sorted_connections(connection.mock_calls[0].args[0])
        self.assertEqual(seen, self._sorted_connections(connection_params))

    def test_init_array(self):
        """Test MessageQueue with a host array."""
        hosts = [f'host{i}' for i in range(32)]
        self._test_init_multiple(hosts, hosts)

    def test_init_string_multiple_space(self):
        """Test MessageQueue with a space-delimited string."""
        hosts = [f'host{i}' for i in range(32)]
        self._test_init_multiple(' '.join(hosts), hosts)

    def test_init_string_multiple_spaces(self):
        """Test MessageQueue with a multi-space-delimited string."""
        hosts = [f'host{i}' for i in range(32)]
        self._test_init_multiple('  '.join(hosts), hosts)

    def test_init_string_multiple_lf(self):
        """Test MessageQueue with a LF-delimited string."""
        hosts = [f'host{i}' for i in range(32)]
        self._test_init_multiple('\n'.join(hosts), hosts)

    @mock.patch('pika.BlockingConnection')
    def test_connect(self, blockingconnection):
        # pylint: disable=no-self-use
        """Test connection parameters."""
        queue = messagequeue.MessageQueue('host', 123, 'user', 'password')

        with queue.connect():
            blockingconnection.assert_called_with(queue.connection_params)

    @mock.patch('pika.ConnectionParameters')
    def test_connection_params(self, connection_params):
        # pylint: disable=no-self-use
        """Test connection params kwarg on init."""
        queue = messagequeue.MessageQueue(
            'host', 123, 'user', 'password',
            connection_params={'extraparam': 'value',
                               'someotherparam': 'othervalue'}
        )
        connection_params.assert_called_with(
            credentials=queue.credentials, host='host', port=123,
            extraparam='value', someotherparam='othervalue'
        )

    def test_connect_context_manager(self):
        # pylint: disable=protected-access
        """Test connect with and without keepalive."""
        queue = messagequeue.MessageQueue('host', 123, 'user', 'password')
        queue._connect_no_keepalive = mock.Mock()
        queue._connect_and_keepalive = mock.Mock()

        queue.keepalive_s = 0
        queue.connect()
        self.assertTrue(queue._connect_no_keepalive.called)

        queue.keepalive_s = 1
        queue.connect()
        self.assertTrue(queue._connect_and_keepalive.called)

    @mock.patch('pika.BlockingConnection')
    def test_connect_context_manager_no_keepalive(self, connection):
        # pylint: disable=protected-access
        """Test context manager."""
        queue = messagequeue.MessageQueue('host', 123, 'user', 'password')

        with queue.connect():
            connection.assert_called_with(queue.connection_params)
        self.assertTrue(connection().close.called)

    @mock.patch('pika.BlockingConnection')
    def test_connect_context_manager_keepalive(self, connection):
        # pylint: disable=protected-access
        """Test context manager."""
        queue = messagequeue.MessageQueue('host', 123, 'user', 'password',
                                          keepalive_s=0.001)
        queue._disconnect_timer = mock.Mock()

        with queue.connect():
            connection.assert_called_with(queue.connection_params)
        self.assertTrue(queue._disconnect_timer.start.called)

    @mock.patch('pika.BlockingConnection')
    def test_connect_keepalive(self, connection):
        """Test keepalive saves connection calls."""
        queue = messagequeue.MessageQueue('host', 123, 'user', 'password',
                                          keepalive_s=0.1)

        self.assertEqual(0, connection.call_count)
        with queue.connect():
            pass
        self.assertEqual(1, connection.call_count)
        with queue.connect():
            pass
        self.assertEqual(1, connection.call_count)
        time.sleep(0.2)
        with queue.connect():
            pass
        self.assertEqual(2, connection.call_count)

    def test_disconnect(self):
        # pylint: disable=protected-access
        """Test _disconnect call."""
        queue = messagequeue.MessageQueue('host', 123, 'user', 'password')
        channel = mock.Mock()
        queue._channel = channel
        queue._disconnect()
        self.assertTrue(channel.connection.close.called)
        self.assertIsNone(queue._channel)

    @staticmethod
    def test_disconnect_reentrant():
        # pylint: disable=protected-access
        """Test _disconnect call."""
        queue = messagequeue.MessageQueue('host', 123, 'user', 'password')
        queue.connect()
        queue._disconnect()
        queue._disconnect()

    @mock.patch('pika.BlockingConnection', mock.Mock())
    def test_disconnect_timer(self):
        # pylint: disable=protected-access
        """Test _disconnect_timer disconnects."""
        queue = messagequeue.MessageQueue('host', 123, 'user', 'password',
                                          keepalive_s=0.1)
        with queue.connect():
            pass

        # As _disconnect_timer is created at initialization,
        # _disconnect_timer is not simple to mock. Instead of checking
        # _disconnect_timer call, check for the result of it.
        self.assertIsNotNone(queue._channel)
        time.sleep(0.2)
        self.assertIsNone(queue._channel)

    @mock.patch('pika.BlockingConnection')
    def test_queue_init(self, connection):  # pylint: disable=no-self-use
        """Test queue_init."""
        queue = messagequeue.MessageQueue('host', 123, 'user', 'password')

        queue.queue_init('queue_name')
        connection().channel().queue_declare.assert_called_with('queue_name')

        queue.queue_init('queue_name', params={'durable': True})
        connection().channel().queue_declare.assert_called_with('queue_name',
                                                                durable=True)

    @mock.patch('pika.BlockingConnection')
    def test_send_message(self, connection):  # pylint: disable=no-self-use
        """Test send_message."""
        data = {'key': 'value'}
        queue_name = 'queue_name'
        exchange_name = 'exchange_name'
        headers = {'header-key': 'value'}
        queue = messagequeue.MessageQueue('host', 123, 'user', 'password')
        properties = pika.BasicProperties(delivery_mode=2,
                                          headers=headers)

        queue.send_message(
            data, queue_name,
            exchange=exchange_name,
            headers=headers,
        )
        connection().channel().basic_publish.assert_called_with(
            body=json.dumps(data),
            exchange=exchange_name,
            routing_key=queue_name,
            properties=properties,
        )

        # Test default exchange.
        queue.send_message(data, queue_name, headers=headers)
        connection().channel().basic_publish.assert_called_with(
            body=json.dumps(data), exchange='', routing_key=queue_name,
            properties=properties,
        )

    @mock.patch('pika.BlockingConnection')
    def test_consume(self, connection):
        """Test consume_messages."""
        self._test_consume(connection)

    @mock.patch('pika.BlockingConnection')
    def test_consume_prefetch(self, connection):
        """Test consume_messages."""
        self._test_consume(connection, prefetch_count=10)

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch('pika.BlockingConnection')
    def test_consume_production_no_queue(self, connection):
        """Test consume_messages."""
        self._test_consume(connection)

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch('pika.BlockingConnection')
    def test_consume_production_queue(self, connection):
        """Test consume_messages."""
        self._test_consume(connection, queue_name='queue',
                           is_production=True)

    @mock.patch('pika.BlockingConnection')
    def test_consume_no_production_queue(self, connection):
        """Test consume_messages."""
        self._test_consume(connection, queue_name='queue',
                           is_production=False)

    @mock.patch('pika.BlockingConnection')
    def test_consume_inactivity_timeout(self, connection):
        """Test consume_messages."""
        self._test_consume(connection, inactivity_timeout=60)

    @mock.patch('pika.BlockingConnection')
    def test_consume_inactivity_timeout_no_return(self, connection):
        """Test consume_messages. Inactivity timeout does not return."""
        self._test_consume(connection, inactivity_timeout=60,
                           return_on_timeout=False)

    @staticmethod
    @mock.patch('pika.BlockingConnection')
    def test_consume_messages_routing_key(connection):
        """Test consume_messages routing_key allows str type."""
        queue = messagequeue.MessageQueue('host', 123, 'user', 'password')
        queue.consume_messages('exchange', 'routing', None)
        connection().channel().queue_bind.assert_has_calls([
            mock.call(mock.ANY, mock.ANY, routing_key='routing')
        ])

    @mock.patch('pika.BlockingConnection')
    def test_consume_messages_terminate_consumer(self, connection):
        """Test the main thread signal handling during consuming."""
        connection().channel().consume.return_value = itertools.repeat(
            (mock.Mock(routing_key='a', delivery_tag='b'),
             'properties', json.dumps({'c': 'd'})))
        callback = mock.Mock(side_effect=lambda a, b: time.sleep(5))
        queue = messagequeue.MessageQueue('host', 123, 'user', 'password')
        pid = os.getpid()
        threading.Thread(target=lambda: time.sleep(0.1) or
                         os.kill(pid, signal.SIGINT)).start()
        with self.assertRaises(KeyboardInterrupt):
            queue.consume_messages('exchange', 'routing', callback)

    @staticmethod
    def _failing_consume():
        for _ in range(3):
            yield (mock.Mock(routing_key='a', delivery_tag='b'), 'properties',
                   json.dumps({'c': 'd'}))
        time.sleep(0.1)
        raise Exception('boom')

    @mock.patch('pika.BlockingConnection')
    def test_consume_messages_terminate_producer(self, connection):
        """Test the handling of an error in consume."""
        connection().channel().consume.return_value = self._failing_consume()
        queue = messagequeue.MessageQueue('host', 123, 'user', 'password')
        callback = mock.Mock()
        queue.consume_messages('exchange', 'routing', callback)
        self.assertEqual(len(callback.mock_calls), 3)

    @mock.patch('pika.BlockingConnection')
    def test_consume_messages_terminate_producer_early(self, connection):
        """Test the early return for an error in consume."""
        connection().channel().consume.return_value = self._failing_consume()
        queue = messagequeue.MessageQueue('host', 123, 'user', 'password')
        callback = mock.Mock(side_effect=lambda a, b: time.sleep(1))
        queue.consume_messages('exchange', 'routing', callback)
        self.assertEqual(len(callback.mock_calls), 1)

    @staticmethod
    def _basic_consume():
        for i in range(3):
            yield (mock.Mock(routing_key='a', delivery_tag=str(i)),
                   'properties', json.dumps({'c': 'd'}))
        yield (None, None, None)

    @staticmethod
    def _manual_side_effect(ack_fns, ack_fn, counters):
        ack_fns.append(ack_fn)
        for _ in range(counters.pop(0)):
            ack_fns.pop(0)()

    def _manual_ack(self, connection, expected_count, counts):
        connection().channel().consume.return_value = self._basic_consume()
        queue = messagequeue.MessageQueue('host', 123, 'user', 'password')
        ack_fns = []
        callback = mock.Mock(side_effect=lambda _, __, ack_fn:
                             self._manual_side_effect(ack_fns, ack_fn, counts))
        queue.consume_messages('exchange', 'routing',
                               callback, manual_ack=True,
                               return_on_timeout=False)
        for callback_call in (connection().channel().connection
                              .add_callback_threadsafe.mock_calls):
            callback_call.args[0]()
        self.assertEqual(len(connection().channel().basic_ack.mock_calls),
                         expected_count)

    @mock.patch('pika.BlockingConnection')
    def test_consume_messages_manual_ack(self, connection):
        """Test the manual acks."""
        self._manual_ack(connection, 3, [1, 1, 1, 0])

    @mock.patch('pika.BlockingConnection')
    def test_consume_messages_manual_ack_none(self, connection):
        """Test the manual acks."""
        self._manual_ack(connection, 0, [0, 0, 0, 0])

    @mock.patch('pika.BlockingConnection')
    def test_consume_messages_manual_ack_final(self, connection):
        """Test the manual acks."""
        self._manual_ack(connection, 3, [0, 0, 0, 3])

    def _test_consume(self, connection, prefetch_count=None,
                      queue_name=None, is_production=False,
                      inactivity_timeout=None,
                      return_on_timeout=True):
        # pylint: disable=too-many-arguments
        exchange = 'exchange'
        routing_keys = ['routing1', 'routing2']
        values = ['value1', 'value2']
        connection().channel().consume.return_value = [
            (mock.Mock(routing_key=f'r{v}', delivery_tag=f't{v}'),
             'properties', json.dumps({'b': v})) for v in values]
        if inactivity_timeout:
            connection().channel().consume.return_value += [(None, None, None)]
            if return_on_timeout:
                connection().channel().consume.return_value += [('invalid',)]
        callback = mock.Mock(side_effect=[Exception('boom'), mock.DEFAULT])

        queue = messagequeue.MessageQueue('host', 123, 'user', 'password')
        with self.assertLogs(messagequeue.LOGGER, 'INFO') as log:
            queue.consume_messages(exchange, routing_keys, callback,
                                   prefetch_count=prefetch_count,
                                   queue_name=queue_name,
                                   inactivity_timeout=inactivity_timeout,
                                   return_on_timeout=return_on_timeout)

        self.assertTrue(any(m.startswith('INFO') for m in log.output))
        connection.assert_called_with(queue.connection_params)
        if prefetch_count:
            connection().channel().basic_qos.assert_called_with(
                prefetch_count=prefetch_count)
        if is_production and queue_name:
            connection().channel().queue_declare.assert_called_with(
                queue_name, durable=True)
        else:
            connection().channel().queue_declare.assert_called_with(
                mock.ANY, auto_delete=True)
        connection().channel().queue_bind.assert_has_calls([
            mock.call(mock.ANY, exchange, routing_key=r)
            for r in routing_keys])
        if inactivity_timeout:
            connection().channel().consume.assert_called_with(
                mock.ANY, inactivity_timeout=inactivity_timeout)
        callback_calls = (connection().channel().connection
                          .add_callback_threadsafe.mock_calls)
        self.assertEqual(len(callback_calls), 1)
        callback_calls[0].args[0]()
        self.assertEqual(connection().channel().basic_ack.mock_calls,
                         [mock.call(f't{values[1]}')])

        callback_calls = [mock.call(f'r{v}', {'b': v}) for v in values]
        if not return_on_timeout:
            callback_calls.append(
                mock.call(None, None)
            )

        self.assertEqual(callback.mock_calls, callback_calls)


class TestMessage(unittest.TestCase):
    """Test the message helpers."""

    def test_gitlab_url_job(self):
        """Test gitlab instance url from job events."""
        self.assertEqual(messagequeue.Message({
            'object_kind': 'build',
            'repository': {
                'homepage': 'https://host.name:1234/group/subgroup/project',
            }
        }).gitlab_url(), 'https://host.name:1234')

    def test_gitlab_url_pipeline(self):
        """Test gitlab instance url from pipeline events."""
        self.assertEqual(messagequeue.Message({
            'object_kind': 'pipeline',
            'project': {
                'web_url': 'https://host.name:1234/group/subgroup/project',
            },
        }).gitlab_url(), 'https://host.name:1234')

    @mock.patch('cki_lib.gitlab.get_instance')
    def test_gitlab_instance(self, get_instance):
        """Test gitlab instance can be returned."""
        get_instance.return_value = 'dummy'
        self.assertEqual(messagequeue.Message({
            'object_kind': 'pipeline',
            'project': {
                'web_url': 'https://host.name:1234/group/subgroup/project',
            },
        }).gl_instance(), 'dummy')
        get_instance.assert_called_with('https://host.name:1234')
