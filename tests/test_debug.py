"""Gitlab api interaction tests."""
import io
import unittest
from unittest import mock

from cki_lib.debug import mark


class TestDebug(unittest.TestCase):
    """Test debug class."""

    @mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_mark(self, mock_stdout):
        """Test mark() behavior."""
        # mark sure variable i doesn't initially exist
        with self.assertRaises(AttributeError):
            _ = mark.i

        # make sure i in mark is initialized to 1 after call
        mark()
        self.assertEqual(mark.i, 1)

        # make sure i in mark is intermented after call
        mark()
        self.assertEqual(mark.i, 2)

        # make sure something was printed
        self.assertTrue('marker' in mock_stdout.getvalue())
