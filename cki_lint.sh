#!/bin/bash
set -euo pipefail

# pass the packages to test (without tests directory) as args to the script

function say { echo "$@" | toilet -f mono12 -w 300 | lolcat -f || echo "$@"; }
function echo_green { echo -e "\e[1;32m${1}\e[0m"; }
function echo_red { echo -e "\e[1;31m${1}\e[0m"; }
function echo_yellow { echo -e "\e[1;33m${1}\e[0m"; }

function did_coverage_decrease() {
    local pipelines_json pipeline_id pipeline_json old_coverage new_coverage=$1
    echo -n "  Getting last successful default branch pipeline... "
    if ! [ -v CI_MERGE_REQUEST_PROJECT_ID ]; then
        echo_yellow "skipped because not in CI job"
        return 1
    fi
    pipelines_json=$(curl -Ss "$CI_API_V4_URL/projects/$CI_MERGE_REQUEST_PROJECT_ID/pipelines?ref=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME&status=success")
    if [ "$(jq '. | length' <<< "$pipelines_json")" -eq 0 ]; then
        echo_yellow "skipped because no successful pipeline on target branch found"
        return 1
    fi
    pipeline_id=$(jq '.[0].id' <<< "$pipelines_json")
    echo_green " $pipeline_id"
    echo -n "  Getting old coverage... "
    pipeline_json=$(curl -Ss "$CI_API_V4_URL/projects/$CI_MERGE_REQUEST_PROJECT_ID/pipelines/$pipeline_id")
    old_coverage=$(jq -r '.coverage // "0.00"' <<< "$pipeline_json")
    if python -c "exit($new_coverage < $old_coverage)"; then
        echo_green "$old_coverage%"
        return 1
    else
        echo_red "$old_coverage% higher than new $new_coverage%"
        return 0
    fi
}

say "preparing"

PACKAGES=(flake8 pydocstyle pylint pytest coverage)

if [[ "$(type -P python3)" = /usr* ]]; then
    python3 -m pip install --user "${PACKAGES[@]}"
else
    python3 -m pip install "${PACKAGES[@]}"
fi

say "linting"

FAILED=()

# Check ALL Python files with flake8 (which includes pycodestyle) and pydocstyle

echo_yellow "Running flake8"
if ! flake8 --exclude ".*,migrations" .; then
    FAILED+=(flake8)
fi

echo_yellow "Running pydocstyle"
if ! pydocstyle --match-dir='^(?!(\.|migrations)).*'; then
    FAILED+=(pydocstyle)
fi

# Only run pylint and coverage for the specified packages

echo_yellow "Running pylint"
if [ -f setup.cfg ]; then
    pylint_args=(--rcfile setup.cfg)
else
    pylint_args=()
fi
if ! pylint "${pylint_args[@]}" "$@"; then
    FAILED+=(pylint)
fi

if [ -f tests/__init__.py ]; then
    echo_yellow "Running pytests under coverage"
    if ! coverage run --source "$(IFS=,; echo "$*")" --branch -m pytest --junitxml=coverage/junit.xml --ignore inttests/ --color=yes -v -r s; then
        FAILED+=(pytest)
    fi

    echo_yellow "Generating coverage reports"
    coverage report -m || true
    coverage html -d coverage/ || true
    coverage xml -o coverage/coverage.xml || true

    echo_yellow "Total coverage and trend"
    new_coverage=$(coverage json -o - | jq .totals.percent_covered)
    echo "  COVERAGE: $new_coverage%"

    did_coverage_decrease "$new_coverage" &
    wait $! && coverage_decreased=1 || coverage_decreased=0
    if [ "$coverage_decreased" = "1" ]; then
        : # FAILED+=(coverage) for now, don't do anything
    fi

fi

if [ "${#FAILED[@]}" -gt 0 ]; then
    echo_red "Failed linting steps: ${FAILED[*]}"
    exit 1
fi

